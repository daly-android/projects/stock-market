package com.daly.stockmarket.di

import com.daly.stockmarket.core.implementations.ResourcesRepositoryImpl
import com.daly.stockmarket.core.interfaces.ResourcesRepository
import com.daly.stockmarket.feature_stock.data.csv.CSVParser
import com.daly.stockmarket.feature_stock.data.csv.CompanyListingsParser
import com.daly.stockmarket.feature_stock.data.csv.IntradayInfoParser
import com.daly.stockmarket.feature_stock.data.repositories.StockRepositoryImpl
import com.daly.stockmarket.feature_stock.domain.models.CompanyListing
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo
import com.daly.stockmarket.feature_stock.domain.repositories.StockRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoriesModule {

    @Binds
    @Singleton
    abstract fun bindCompanyListingsParser(
        companyListingsParser: CompanyListingsParser
    ): CSVParser<CompanyListing>

    @Binds
    @Singleton
    abstract fun bindIntradayInfoParser(
        intradayInfoParser: IntradayInfoParser
    ): CSVParser<IntradayInfo>

    @Binds
    @Singleton
    abstract fun bindStockRepository(
        stockRepositoryImpl: StockRepositoryImpl
    ): StockRepository

    @Binds
    @Singleton
    abstract fun bindResourcesRepository(
        resourcesRepositoryImpl: ResourcesRepositoryImpl
    ): ResourcesRepository
}
