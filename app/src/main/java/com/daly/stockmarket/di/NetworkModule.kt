package com.daly.stockmarket.di

import com.daly.stockmarket.core.network.OkHttpClientBase
import com.daly.stockmarket.core.network.OkHttpInterceptors
import com.daly.stockmarket.core.network.RetrofitBuilder
import com.daly.stockmarket.feature_stock.data.remote.apis.StockApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitClient(moshi: Moshi, okHttpClient: OkHttpClient, okHttpInterceptors: OkHttpInterceptors): StockApi {
        return RetrofitBuilder(moshi, okHttpClient, okHttpInterceptors).build(StockApi::class.java)
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClientBase.buildOkHttpClient()

    @Provides
    @Singleton
    fun provideOkHttpInterceptors(): OkHttpInterceptors = OkHttpInterceptors()
}
