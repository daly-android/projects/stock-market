package com.daly.stockmarket.di

import android.app.Application
import androidx.room.Room
import com.daly.stockmarket.core.implementations.DispatcherServiceImpl
import com.daly.stockmarket.core.interfaces.DispatcherService
import com.daly.stockmarket.feature_stock.data.local.StockDatabase
import com.daly.stockmarket.feature_stock.data.local.StockDatabase.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

//    @Provides
//    @Singleton
//    fun provideGetPersonUseCase(repository: StockRepository): GetPersonUseCase {
//        return GetPersonUseCase(repository)
//    }

    @Provides
    @Singleton
    fun provideNoteDatabase(app: Application): StockDatabase {
        return Room.databaseBuilder(
            context = app,
            klass = StockDatabase::class.java,
            name = DATABASE_NAME
        ).build()
    }

    @Provides
    fun provideDispatcherPService(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
