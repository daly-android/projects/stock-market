package com.daly.stockmarket.feature_stock.presentation.company_info

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.stockmarket.core.interfaces.DispatcherService
import com.daly.stockmarket.feature_stock.domain.models.CompanyInfo
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo
import com.daly.stockmarket.feature_stock.domain.repositories.StockRepository
import com.daly.stockmarket.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CompanyInfoViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val stockRepository: StockRepository,
    dispatcherService: DispatcherService
) : ViewModel() {

    private val _uiState: MutableStateFlow<CompanyInfoUiState> = MutableStateFlow(CompanyInfoUiState.Loading)
    val uiState: StateFlow<CompanyInfoUiState> = _uiState.stateIn(
        initialValue = CompanyInfoUiState.Loading,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    init {
        viewModelScope.launch(dispatcherService.io) {
            val symbol = savedStateHandle.get<String>("symbol") ?: return@launch
            val deferredResults = listOf(
                async { stockRepository.getCompanyInfo(symbol) },
                async { stockRepository.getIntradayInfo(symbol) }
            )

            var companyInfo: CompanyInfo? = null
            var intradayInfo: List<IntradayInfo> = emptyList()

            val results = awaitAll(*deferredResults.toTypedArray())
            results.forEachIndexed { index, result ->
                when (result) {
                    is Resource.Error -> {
                        _uiState.value = CompanyInfoUiState.Error(message = result.message)
                        return@launch
                    }
                    is Resource.Loading -> return@launch
                    is Resource.Success -> {
                        if (index == 0) {
                            companyInfo = (result.data as? CompanyInfo)
                        } else {
                            intradayInfo = (result.data as List<IntradayInfo>)
                        }
                    }
                }
            }
            _uiState.value = CompanyInfoUiState.Success(Pair(intradayInfo, companyInfo))
        }
    }
}
