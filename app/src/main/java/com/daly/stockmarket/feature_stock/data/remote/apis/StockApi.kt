package com.daly.stockmarket.feature_stock.data.remote.apis

import com.daly.stockmarket.core.configuration.ApiConfiguration
import com.daly.stockmarket.feature_stock.data.remote.dto.CompanyInfoDto
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface StockApi {

    @GET("query?function=LISTING_STATUS")
    suspend fun getListings(
        @Query("apikey") apiKey: String = ApiConfiguration.mzFAdgRwoX()
    ): ResponseBody

    @GET("query?function=TIME_SERIES_INTRADAY&interval=60min&datatype=csv")
    suspend fun getIntradayInfo(
        @Query("symbol") symbol: String,
        @Query("apikey") apiKey: String = ApiConfiguration.mzFAdgRwoX()
    ): ResponseBody

    @GET("query?function=OVERVIEW")
    suspend fun getCompanyInfo(
        @Query("symbol") symbol: String,
        @Query("apikey") apiKey: String = ApiConfiguration.mzFAdgRwoX()
    ): CompanyInfoDto
}
