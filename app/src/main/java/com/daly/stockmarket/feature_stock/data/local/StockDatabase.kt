package com.daly.stockmarket.feature_stock.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [CompanyListingEntity::class],
    version = 1
)
abstract class StockDatabase : RoomDatabase() {
    abstract val dao: StockDao

    companion object {
        const val DATABASE_NAME = "companies_stock_db"
    }
}
