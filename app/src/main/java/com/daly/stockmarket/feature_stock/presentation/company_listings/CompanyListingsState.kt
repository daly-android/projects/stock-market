package com.daly.stockmarket.feature_stock.presentation.company_listings

import com.daly.stockmarket.feature_stock.domain.models.CompanyListing

data class CompanyListingsState(
    val companies: List<CompanyListing> = emptyList(),
    val isLoading: Boolean = false,
    val searchQuery: String = ""
)
