package com.daly.stockmarket.feature_stock.presentation.company_listings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.stockmarket.core.interfaces.DispatcherService
import com.daly.stockmarket.feature_stock.domain.repositories.StockRepository
import com.daly.stockmarket.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class CompanyListingsViewModel @Inject constructor(
    private val stockRepository: StockRepository,
    private val dispatcherService: DispatcherService
) : ViewModel() {

    private val _uiState = MutableStateFlow(CompanyListingsState())
    val uiState: StateFlow<CompanyListingsState> = _uiState.stateIn(
        initialValue = CompanyListingsState(),
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    private var searchJob: Job? = null

    init {
        getCompanyListings()
    }

    fun onEvent(event: CompanyListingsEvent) = when (event) {
        CompanyListingsEvent.Refresh -> {
            getCompanyListings(fetchFromRemote = true)
        }

        is CompanyListingsEvent.OnSearchQueryChange -> {
            _uiState.value = _uiState.value.copy(searchQuery = event.query)
            searchJob?.cancel()
            searchJob = viewModelScope.launch {
                delay(500L)
                getCompanyListings()
            }
        }
    }

    private fun getCompanyListings(
        fetchFromRemote: Boolean = false,
        query: String = _uiState.value.searchQuery.lowercase(Locale.getDefault())
    ) {
        viewModelScope.launch(dispatcherService.io) {
            stockRepository
                .getCompanyListings(fetchFromRemote, query)
                .collect { result ->
                    withContext(dispatcherService.main) {
                        when (result) {
                            is Resource.Error -> Unit

                            is Resource.Loading -> {
                                _uiState.value = _uiState.value.copy(isLoading = result.isLoading)
                            }

                            is Resource.Success -> {
                                result.data?.let { listings ->
                                    _uiState.value = _uiState.value.copy(companies = listings)
                                }
                            }
                        }
                    }
                }
        }
    }
}
