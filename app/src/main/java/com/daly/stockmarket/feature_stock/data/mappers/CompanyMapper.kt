package com.daly.stockmarket.feature_stock.data.mappers

import com.daly.stockmarket.feature_stock.data.local.CompanyListingEntity
import com.daly.stockmarket.feature_stock.data.remote.dto.CompanyInfoDto
import com.daly.stockmarket.feature_stock.domain.models.CompanyInfo
import com.daly.stockmarket.feature_stock.domain.models.CompanyListing

fun CompanyListingEntity.toCompanyListing(): CompanyListing = CompanyListing(
    name = name,
    symbol = symbol,
    exchange = exchange
)

fun CompanyListing.toCompanyListingEntity(): CompanyListingEntity = CompanyListingEntity(
    name = name,
    symbol = symbol,
    exchange = exchange
)

fun CompanyInfoDto.toCompanyInfo() = CompanyInfo(
    symbol = symbol ?: "",
    description = description ?: "",
    name = name ?: "",
    country = country ?: "",
    industry = industry ?: ""
)
