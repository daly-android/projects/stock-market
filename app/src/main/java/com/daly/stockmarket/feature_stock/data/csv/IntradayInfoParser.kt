package com.daly.stockmarket.feature_stock.data.csv

import com.daly.stockmarket.feature_stock.data.mappers.toIntradayInfo
import com.daly.stockmarket.feature_stock.data.remote.dto.IntradayInfoDto
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo
import com.opencsv.CSVReader
import java.io.InputStream
import java.io.InputStreamReader
import java.time.LocalDate
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class IntradayInfoParser @Inject constructor() : CSVParser<IntradayInfo> {

    override suspend fun parse(stream: InputStream): List<IntradayInfo> {
        val csvReader = CSVReader(InputStreamReader(stream))
        return csvReader
            .readAll()
            .drop(1)
            .mapNotNull { line ->
                val timestamp = line.getOrNull(0) ?: return@mapNotNull null
                val close = line.getOrNull(4) ?: return@mapNotNull null
                val dto = IntradayInfoDto(timestamp, close.toDouble())
                dto.toIntradayInfo()
            }
            .filter {
                it.date.dayOfMonth == LocalDate.now().minusDays(4).dayOfMonth
            }
            .sortedBy {
                it.date.hour
            }
            .also {
                csvReader.close()
            }
    }
}
