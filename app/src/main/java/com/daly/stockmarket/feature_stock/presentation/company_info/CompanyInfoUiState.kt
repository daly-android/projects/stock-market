package com.daly.stockmarket.feature_stock.presentation.company_info

import com.daly.stockmarket.feature_stock.domain.models.CompanyInfo
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo

sealed interface CompanyInfoUiState {
    data class Success(val info: Pair<List<IntradayInfo>, CompanyInfo?>) : CompanyInfoUiState
    data object Loading : CompanyInfoUiState
    data class Error(val message: String?) : CompanyInfoUiState
}
