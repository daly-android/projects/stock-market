package com.daly.stockmarket.feature_stock.domain.repositories

import com.daly.stockmarket.feature_stock.domain.models.CompanyInfo
import com.daly.stockmarket.feature_stock.domain.models.CompanyListing
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo
import com.daly.stockmarket.utils.Resource
import kotlinx.coroutines.flow.Flow

interface StockRepository {

    suspend fun getCompanyListings(
        fetchFromRemote: Boolean,
        query: String
    ): Flow<Resource<List<CompanyListing>>>

    suspend fun getIntradayInfo(
        symbol: String
    ): Resource<List<IntradayInfo>>

    suspend fun getCompanyInfo(
        symbol: String
    ): Resource<CompanyInfo>
}
