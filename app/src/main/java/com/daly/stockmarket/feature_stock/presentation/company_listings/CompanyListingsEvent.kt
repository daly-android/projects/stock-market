package com.daly.stockmarket.feature_stock.presentation.company_listings

sealed interface CompanyListingsEvent {
    data object Refresh : CompanyListingsEvent
    data class OnSearchQueryChange(val query: String) : CompanyListingsEvent
}
