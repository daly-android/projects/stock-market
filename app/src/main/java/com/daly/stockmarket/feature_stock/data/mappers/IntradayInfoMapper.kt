package com.daly.stockmarket.feature_stock.data.mappers

import com.daly.stockmarket.feature_stock.data.remote.dto.IntradayInfoDto
import com.daly.stockmarket.feature_stock.domain.models.IntradayInfo
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

private const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss"

fun IntradayInfoDto.toIntradayInfo(): IntradayInfo {
    val formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN, Locale.getDefault())
    val localDateTime = LocalDateTime.parse(timestamp, formatter)
    return IntradayInfo(
        date = localDateTime,
        close = close
    )
}
