package com.daly.stockmarket.core.implementations

import android.content.Context
import androidx.annotation.StringRes
import com.daly.stockmarket.core.interfaces.ResourcesRepository
import javax.inject.Inject

class ResourcesRepositoryImpl @Inject constructor(private val appContext: Context) : ResourcesRepository {

    override fun fetchString(@StringRes id: Int, vararg args: Any?): String = appContext.getString(id, *args)
}
