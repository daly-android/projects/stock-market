package com.daly.stockmarket.core.interfaces

/**
 * Domain interface to fetch resources
 */
interface ResourcesRepository {

    /**
     * Fetch a string according to an id
     * @param id The identifier of the string resources, may be a R.string or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchString(id: Int, vararg args: Any?): String
}
