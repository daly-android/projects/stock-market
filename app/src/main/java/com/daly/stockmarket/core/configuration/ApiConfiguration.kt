package com.daly.stockmarket.core.configuration

object ApiConfiguration {

    // Get API key
    fun mzFAdgRwoX(): String {
        val obfuscated = intArrayOf(0x44, 0x26, 0x06, 0x1d, 0x21, 0x53, 0x3d, 0x06, 0x43, 0xc6, 0xe4, 0x0a, 0x0a, 0x52, 0xb3, 0x1c) // DVUIR94MBL4PJD73
        val result = ByteArray(16)
        result[0] = obfuscated[6].plus(7).toByte()
        result[1] = obfuscated[5].xor(5).toByte()
        result[2] = obfuscated[9].minus(113).toByte()
        result[3] = obfuscated[8].plus(6).toByte()
        result[4] = obfuscated[12].shl(3).plus(2).toByte()
        result[5] = obfuscated[15].shl(1).plus(1).toByte()
        result[6] = obfuscated[3].plus(23).toByte()
        result[7] = obfuscated[1].shl(1).plus(1).toByte()
        result[8] = obfuscated[4].shl(1).plus(0).toByte()
        result[9] = obfuscated[14].minus(103).toByte()
        result[10] = obfuscated[2].shl(3).plus(4).toByte()
        result[11] = obfuscated[13].xor(2).toByte()
        result[12] = obfuscated[10].minus(154).toByte()
        result[13] = obfuscated[0].xor(0).toByte()
        result[14] = obfuscated[11].plus(45).toByte()
        result[15] = obfuscated[7].shl(3).plus(3).toByte()
        return result.toString(Charsets.UTF_8)
    }
}
