package com.daly.stockmarket.core.network

import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

class OkHttpLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        Timber.v(message)
    }
}
