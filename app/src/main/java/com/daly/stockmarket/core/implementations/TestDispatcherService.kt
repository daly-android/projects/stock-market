package com.daly.stockmarket.core.implementations

import com.daly.stockmarket.core.interfaces.DispatcherService
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/** Test dispatcher service link only to Main dispatcher. */
class TestDispatcherService : DispatcherService {
    override val io: CoroutineContext get() = Dispatchers.Main
    override val main: CoroutineContext get() = Dispatchers.Main
    override val default: CoroutineContext get() = Dispatchers.Main
    override val unconfined: CoroutineContext get() = Dispatchers.Main
}
