package com.daly.stockmarket.core.network

import com.daly.stockmarket.BuildConfig
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpInterceptors {

    fun buildDynamic(): List<Interceptor> = listOfNotNull(
        buildHttpLoggingInterceptor()
    )

    private fun buildHttpLoggingInterceptor() = HttpLoggingInterceptor(logger = OkHttpLogger()).apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }
}
