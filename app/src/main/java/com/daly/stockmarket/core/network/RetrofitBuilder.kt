package com.daly.stockmarket.core.network

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import timber.log.Timber

class RetrofitBuilder(
    private val moshi: Moshi,
    private val okHttpClient: OkHttpClient,
    private val okHttpInterceptors: OkHttpInterceptors
) {

    private companion object {
        const val BASE_URL = "https://www.alphavantage.co/"
    }

    fun <T> build(apiClass: Class<T>): T {
        Timber.d("RetrofitBuilder.build")
        val client = okHttpClient.newBuilder()
        okHttpInterceptors.buildDynamic().map { client.addInterceptor(it) }
        return buildRetrofitClient(client.build()).create(apiClass)
    }

    private fun buildRetrofitClient(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(ScalarsConverterFactory.create()) // convert to primitives types
        .addConverterFactory(MoshiConverterFactory.create(moshi)) // convert JSON to object
        .build()
}
