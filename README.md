## Stock market App

## Overview
This is an application to provide stock market info and that contain two screens.

The first screen show companies list so user can search for a specific company using a search field.

The second screen show multiple information for a specific company such as symbol, name, description, country, industry.

It show also a graph char that represent company closes for one day.


|                                                                                   |                                                                                 |                                                                               |
|:---------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------:|
|       <img width="1604" alt="companies_list" src="docs/companies_list.png">       |       <img width="1604" alt="serch_company" src="docs/serch_company.png">       |       <img width="1604" alt="company_info" src="docs/company_info.png">       |
| <img width="1604" alt="companies_list_light" src="docs/companies_list_light.png"> | <img width="1604" alt="serch_company_light" src="docs/serch_company_light.png"> | <img width="1604" alt="company_info_light" src="docs/company_info_light.png"> |


## Technical stack :
# 100% Kotlin & Jetpack Compose
# Architecture : MVVM and clean
# Dependency injection : Dagger-Hilt
# Build configuration language : Kotlin DSL
# Local data storage : Room
# Network & API : Retrofit, OkHttp, Moshi
# Parse arrays API response : OpenCSV
# Navigation : raamcosta/compose-destinations library
